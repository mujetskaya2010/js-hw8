///1.Опишіть своїми словами що таке Document Object Model (DOM)

///Це можна сказати дерево обєктів за допомогою якого можна связати веб сторінку і мову програмування. 
///Також завдяки йому можна зручно рекувати стилями, html документов в файлі js.
///Це як міст між язиком програмування і html документом.

///2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?

///innerHTML - в ньому працюють теги і їх властивості наприклад <br>hello<br> зробить текст жирним і без тегу br. 
///А innerText виводить просто як текст те що ми напишем і <br>hello<br> так і виведеться на екран як <br>hello<br> 

///3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

///document.getElementById () - пошук по id. document.getElementsByName () - Метод повертає список всіх елементів, чий атрибут name задовольняє запиту. 
///document.getElementsByClassName () - пошук по красу querySelectorAll ()
///- Метод повертає список всіх елементів, що задовольняють заданій CSS-селектору querySelector () - . 
///Метод повертає перший елемент, що задовольняє заданому CSS-селектору. Найкращі і простіше це querySelector (), querySelectorAll ()


const paragraph = document.querySelectorAll("p");
    for(let elem of paragraph){
elem.style.backgroundColor = "#ff0000";
}

const idElement = document.getElementById("optionsList");
console.log(idElement);

const idParent = idElement.parentElement;
console.log(idParent);

if (idElement.hasChildNodes){
    const idChildren = idElement.childNodes;
    for(let every of idChildren){
    console.log(`Name: ${every.nodeName}, Type: ${every.nodeType}`)
}
}

const classElement = document.createElement("p");
classElement.className = "testParagraph";
classElement.innerText = "This is a paragraph";

const header = document.querySelector(".main-header")
const insideElements = header.getElementsByTagName("*")
console.log(insideElements)
for(let item of insideElements){
    item.className = "nav-item";
}

let title = document.querySelectorAll(".section-title");
title.forEach(deleteClass);

function deleteClass (element){
    element.classList.remove("section-title");
}